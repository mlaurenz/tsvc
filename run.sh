#!/usr/bin/env bash

##
## This script runs the TSVC benchmarks, prints out useful information, checks/formats results.
## The default behavior is to run all tests once. This behavior can be customized via the 
## following environment variables.
##
## TSVC_DTYPES    A list of the datatypes to use (float, double, int64_t, int32_t, int16_t, int8_t)
## TSVC_TESTS     A list of the tests to use (s000, s111, etc.)
## TSVC_VEC       A list of the vectorization options to use (vec, novec)
## TSVC_COUNT     The number of times to run each test
## TSVC_VERIFY    Accuracy verification failures are fatal
## TSVC_GOLD      Generate golden results for accuracy checks
##
##
## Examples:
##
## (generate accuracy checks)
## TSVC_GOLD=1 ./run.sh
##
## (run a small subset of tests, dying with error if accuracy checks fail)
## TSVC_VERIFY=1 TSVC_DTYPES="float double" TSVC_TESTS="s000 s111 s222" ./run.sh
##

goldf=golden_results.csv

function die_error(){
    echo ${1}
    exit 1
}
export -f die_error

function do_exec(){
    bench=${1}
    taskset -c 1 ${bench}
}
export -f do_exec

function run_bench(){
    t=${1}
    d=${2}
    v=${3}
    bench=bin/${1}_${2}_${3}
    if [ ! -f ${bench} ]; then
        die_error "cannot find benchmark executable: ${bench}"
    fi

    x=$(do_exec ${bench} | grep -Pv "Loop")

    tt=$(echo ${x} | awk '{ print $1 }')
    m=$(echo ${x} | awk '{ print $2 }')
    c=$(echo ${x} | awk '{ print $3 }')

    if [ "${tt}" != "${t}" ]; then
        echo ${x}
        die_error "${bench}: test names ${tt}/${t} do not match"
    fi

    gold=$(do_golden get ${t} ${d})

    V="yes"
    if [ "${gold}" != "${c}" ]; then
        V="no"
    fi
    if [ "${V}" == "no" -a "${TSVC_VERIFY}" != "" ]; then
        die_error "${bench}: test checksum ${c} does not match golden value ${gold}"
    fi

    echo ${t} ${d} ${v} ${m} ${V} | tr ' ' '\t'
}
export -f run_bench

function do_golden(){
    cmd=${1}
    t=${2}
    d=${3}
    bench=bin/${t}_${d}_novec

    if [ "${cmd}" == "set" ]; then
        if [ ! -f ${bench} ]; then
            die_error "cannot find benchmark executable: ${bench}"
        fi
        ${bench} | tail -n1 | awk '{ print $3 }' >> ${goldf}
        echo "wrote golden result: ${goldf}"
    elif [ "${cmd}" == "get" ]; then
        if [ ! -f ${goldf} ]; then
            die_error "cannot find golden result for ${bench}... try running this script with TSVC_GOLD=1 to set up golden results"
        fi
        cat ${goldf} | awk -v t=${t} -v d=${d} '{ if ($1 == t && $2 == d) print $3 }'
    else
        die_error "unknown cmd to do_golden"
    fi
}
export -f do_golden


if [ "${TSVC_GOLD}" != "" ]; then
    TSVC_COUNT=1
    TSVC_VEC=novec
fi

if [ "${TSVC_DTYPES}" == "" ]; then
    TSVC_DTYPES=all
fi
if [ "${TSVC_DTYPES}" == "all" ]; then
    TSVC_DTYPES="float double int64_t int32_t int16_t int8_t"
fi

if [ "${TSVC_TESTS}" == "" ]; then
    TSVC_TESTS=all
fi
if [ "${TSVC_TESTS}" == "all" ]; then
    TSVC_TESTS="s000 s111 s1111 s1112 s1113 s1115 s1119 s112 s113 s114 s115 s116 s1161 s118 s119 s121 s1213 s122 s1221 s123 s1232 s124 s1244 s125 s1251 s126 s127 s1279 s128 s1281 s131 s13110 s132 s1351 s141 s1421 s151 s152 s161 s162 s171 s172 s173 s174 s175 s176 s2101 s2102 s211 s2111 s212 s221 s222 s2233 s2244 s2251 s2275 s231 s232 s233 s235 s241 s242 s243 s244 s251 s252 s253 s254 s255 s256 s257 s258 s261 s271 s2710 s2711 s2712 s272 s273 s274 s275 s276 s277 s278 s279 s281 s291 s292 s293 s311 s3110 s3111 s31111 s3112 s3113 s312 s313 s314 s315 s316 s317 s318 s319 s321 s322 s323 s3251 s331 s332 s341 s342 s343 s351 s352 s353 s4112 s4113 s4114 s4115 s4116 s4117 s4121 s421 s422 s423 s424 s431 s441 s442 s443 s451 s452 s453 s471 s481 s482 s491 va vag vas vbor vdotr vif vpv vpvpv vpvts vpvtv vsumr vtv vtvtv"
fi

if [ "${TSVC_VEC}" == "" ]; then
    TSVC_VEC=all
fi
if [ "${TSVC_VEC}" == "all" ]; then
    TSVC_VEC="vec novec"
fi

if [ "${TSVC_COUNT}" == "" ]; then
    TSVC_COUNT=1
fi


if [ "${TSVC_GOLD}" == "" ]; then
    echo "Loop Dtype Vect Time Accurate" | tr ' ' '\t'
    for t in ${TSVC_TESTS}; do
        for d in ${TSVC_DTYPES}; do
            for v in ${TSVC_VEC}; do
                for i in $(seq 1 ${TSVC_COUNT}); do
                    if [ "${TSVC_GOLD}" != "" ]; then
                        do_golden set ${t} ${d}
                    else
                        run_bench ${t} ${d} ${v}
                    fi
                done
            done
        done
    done
else
    echo "Running non-vectorized versions to get golden results..."
    parallel -k --gnu do_golden set {1} {2} ::: ${TSVC_TESTS} ::: ${TSVC_DTYPES}
fi

