## defines architecture-specific vectorization flags
include flags.x86_64

INCLUDE = -I.
libs = -lm
noopt = -O0

BIN = ./bin
OBJ = ./obj
SRC = ./src

I1FLAGS = -DTSVC_DTYPE=int8_t
I2FLAGS = -DTSVC_DTYPE=int16_t
I4FLAGS = -DTSVC_DTYPE=int32_t
I8FLAGS = -DTSVC_DTYPE=int64_t
DPFLAGS = -DTSVC_DTYPE=double
SPFLAGS = -DTSVC_DTYPE=float
flags += -I.

TESTS = s000 s111 s1111 s1112 s1113 s1115 s1119 s112 s113 s114 s115 s116 s1161 s118 s119 s121 s1213 s122 s1221 s123 s1232 s124 s1244 s125 s1251 s126 s127 s1279 s128 s1281 s131 s13110 s132 s1351 s141 s1421 s151 s152 s161 s162 s171 s172 s173 s174 s175 s176 s2101 s2102 s211 s2111 s212 s221 s222 s2233 s2244 s2251 s2275 s231 s232 s233 s235 s241 s242 s243 s244 s251 s252 s253 s254 s255 s256 s257 s258 s261 s271 s2710 s2711 s2712 s272 s273 s274 s275 s276 s277 s278 s279 s281 s291 s292 s293 s311 s3110 s3111 s31111 s3112 s3113 s312 s313 s314 s315 s316 s317 s318 s319 s321 s322 s323 s3251 s331 s332 s341 s342 s343 s351 s352 s353 s4112 s4113 s4114 s4115 s4116 s4117 s4121 s421 s422 s423 s424 s431 s441 s442 s443 s451 s452 s453 s471 s481 s482 s491 va vag vas vbor vdotr vif vpv vpvpv vpvts vpvtv vsumr vtv vtvtv
DTYPES = float double int64_t int32_t int16_t int8_t
#TESTS=s000 s111 s222
#DTYPES=float double

TGTS = $(foreach t,$(TESTS),$(foreach d,$(DTYPES),$(BIN)/$(t)_$(d)_vec   $(BIN)/$(t)_$(d)_novec))
OBJS = $(foreach t,$(TESTS),$(foreach d,$(DTYPES),$(OBJ)/$(t)_$(d)_vec.o $(OBJ)/$(t)_$(d)_novec.o))
SRCS = $(foreach t,$(TESTS),$(SRC)/$(t).c)


.PHONY: all clean.
POBJ = $(foreach d,$(DTYPES),$(OBJ)/%_$(d)_vec.o $(OBJ)/%_$(d)_novec.o)
.PRECIOUS: $(SRC)/%.c $(POBJ)

all: $(TGTS)


## float
$(BIN)/%_float_vec: $(OBJ)/%_float_vec.o $(OBJ)/dummy.o
	$(CC) $(noopt) $^ -o $@ $(libs)

$(BIN)/%_float_novec: $(OBJ)/%_float_novec.o $(OBJ)/dummy.o
	$(CC) $(noopt) $^ -o $@ $(libs)

$(OBJ)/%_float_vec.o: $(SRC)/%.c
	$(CC) $(flags) $(vecflags) $(SPFLAGS) -c -o $@ $^

$(OBJ)/%_float_novec.o: $(SRC)/%.c
	$(CC) $(flags) $(novecflags) $(SPFLAGS) -c -o $@ $^


## double
$(BIN)/%_double_vec: $(OBJ)/%_double_vec.o $(OBJ)/dummy.o
	$(CC) $(noopt) $^ -o $@ $(libs)

$(BIN)/%_double_novec: $(OBJ)/%_double_novec.o $(OBJ)/dummy.o
	$(CC) $(noopt) $^ -o $@ $(libs)

$(OBJ)/%_double_vec.o: $(SRC)/%.c
	$(CC) $(flags) $(vecflags) $(DPFLAGS) -c -o $@ $^

$(OBJ)/%_double_novec.o: $(SRC)/%.c
	$(CC) $(flags) $(novecflags) $(DPFLAGS) -c -o $@ $^


## int64_t
$(BIN)/%_int64_t_vec: $(OBJ)/%_int64_t_vec.o $(OBJ)/dummy.o
	$(CC) $(noopt) $^ -o $@ $(libs)

$(BIN)/%_int64_t_novec: $(OBJ)/%_int64_t_novec.o $(OBJ)/dummy.o
	$(CC) $(noopt) $^ -o $@ $(libs)

$(OBJ)/%_int64_t_vec.o: $(SRC)/%.c
	$(CC) $(flags) $(vecflags) $(I8FLAGS) -c -o $@ $^

$(OBJ)/%_int64_t_novec.o: $(SRC)/%.c
	$(CC) $(flags) $(novecflags) $(I8FLAGS) -c -o $@ $^


## int32_t
$(BIN)/%_int32_t_vec: $(OBJ)/%_int32_t_vec.o $(OBJ)/dummy.o
	$(CC) $(noopt) $^ -o $@ $(libs)

$(BIN)/%_int32_t_novec: $(OBJ)/%_int32_t_novec.o $(OBJ)/dummy.o
	$(CC) $(noopt) $^ -o $@ $(libs)

$(OBJ)/%_int32_t_vec.o: $(SRC)/%.c
	$(CC) $(flags) $(vecflags) $(I4FLAGS) -c -o $@ $^

$(OBJ)/%_int32_t_novec.o: $(SRC)/%.c
	$(CC) $(flags) $(novecflags) $(I4FLAGS) -c -o $@ $^


## int16_t
$(BIN)/%_int16_t_vec: $(OBJ)/%_int16_t_vec.o $(OBJ)/dummy.o
	$(CC) $(noopt) $^ -o $@ $(libs)

$(BIN)/%_int16_t_novec: $(OBJ)/%_int16_t_novec.o $(OBJ)/dummy.o
	$(CC) $(noopt) $^ -o $@ $(libs)

$(OBJ)/%_int16_t_vec.o: $(SRC)/%.c
	$(CC) $(flags) $(vecflags) $(I2FLAGS) -c -o $@ $^

$(OBJ)/%_int16_t_novec.o: $(SRC)/%.c
	$(CC) $(flags) $(novecflags) $(I2FLAGS) -c -o $@ $^


## int8_t
$(BIN)/%_int8_t_vec: $(OBJ)/%_int8_t_vec.o $(OBJ)/dummy.o
	$(CC) $(noopt) $^ -o $@ $(libs)

$(BIN)/%_int8_t_novec: $(OBJ)/%_int8_t_novec.o $(OBJ)/dummy.o
	$(CC) $(noopt) $^ -o $@ $(libs)

$(OBJ)/%_int8_t_vec.o: $(SRC)/%.c
	$(CC) $(flags) $(vecflags) $(I1FLAGS) -c -o $@ $^

$(OBJ)/%_int8_t_novec.o: $(SRC)/%.c
	$(CC) $(flags) $(novecflags) $(I1FLAGS) -c -o $@ $^


## generic
$(SRC)/%.c: gen_driver.py tsvc_routines.h
	./gen_driver.py $@ $*

$(OBJ)/dummy.o:
	$(CC) -c dummy.c -o $@


clean:
	rm -f $(TGTS) $(SRCS) $(OBJS) $(OBJ)/dummy.o

dist-clean:
	rm -f $(BIN)/* $(OBJ)/* $(SRC)/*
