#!/usr/bin/env python

import string
import sys

## argument definitions
n1 = 'n1'
n3 = 'n3'
s1 = 's1'
s2 = 's2'
ip = 'ip'
LENd2 = 'LEN/2'
LEN2d2 = 'LEN2/2'

## test definitions
tests = {
    's000': [],
    's111': [],
    's111v2': [],
    's1111': [],
    's1111v2': [],
    's112': [],
    's112v2': [],
    's1112': [],
    's1112v2': [],
    's113': [],
    's1113': [],
    's114': [],
    's115': [],
    's1115': [],
    's116': [],
    's118': [],
    's119': [],
    's1119': [],
    's121': [],
    's122': [n1, n3],
    's123': [],
    's124': [],
    's125': [],
    's126': [],
    's127': [],
    's128': [],
    's131': [],
    's132': [],
    's141': [],
    's151': [],
    's152': [],
    's152v2': [],
    's161': [],
    's161v2': [],
    's1161': [],
    's162': [n1],
    's171': [n1],
    's172': [n1, n3],
    's173': [],
    's174': [LENd2],
    's175': [n1],
    's176': [],
    's211': [],
    's212': [],
    's1213': [],
    's221': [],
    's1221': [],
    's222': [],
    's231': [],
    's232': [],
    's1232': [],
    's233': [],
    's2233': [],
    's235': [],
    's241': [],
    's242': [s1, s2],
    's243': [],
    's244': [],
    's1244': [],
    's2244': [],
    's251': [],
    's1251': [],
    's2251': [],
    's3251': [],
    's252': [],
    's253': [],
    's254': [],
    's255': [],
    's256': [],
    's257': [],
    's258': [],
    's261': [],
    's271': [],
    's272': [s1],
    's273': [],
    's274': [],
    's275': [],
    's2275': [],
    's276': [],
    's277': [],
    's278': [],
    's279': [],
    's1279': [],
    's2710': [s1],
    's2711': [],
    's2712': [],
    's281': [],
    's1281': [],
    's291': [],
    's292': [],
    's293': [],
    's2101': [],
    's2102': [],
    's2102v2': [],
    's2111': [],
    's311': [],
    's31111': [],
    's312': [],
    's313': [],
    's313v2': [],
    's314': [],
    's315': [],
    's316': [],
    's317': [],
    's318': [n1],
    's319': [],
    's3110': [],
    's13110': [],
    's3111': [],
    's3112': [],
    's3113': [],
    's321': [],
    's322': [],
    's323': [],
    's331': [],
    's332': [s1],
    's341': [],
    's342': [],
    's343': [],
    's351': [],
    's1351': [],
    's352': [],
    's353': [ip],
    's421': [],
    's1421': [],
    's422': [],
    's423': [],
    's424': [],
    's431': [],
    's441': [],
    's442': [],
    's443': [],
    's451': [],
    's452': [],
    's453': [],
    's453v2': [],
    's471': [],
    's481': [],
    's482': [],
    's491': [ip],
    's4112': [ip, s1],
    's4113': [ip],
    's4114': [ip, n1],
    's4115': [ip],
    's4116': [ip, LEN2d2, n1],
    's4117': [],
    's4121': [],
    'va': [],
    'vag': [ip],
    'vas': [ip],
    'vif': [],
    'vpv': [],
    'vtv': [],
    'vpvtv': [],
    'vpvts': [s1],
    'vpvpv': [],
    'vtvtv': [],
    'vsumr': [],
    'vsumrv2': [],
    'vdotr': [],
    'vbor': []
}

def print_usage(msg=None):
    if not msg == None:
        print 'error: %s' % (msg)
    print 'usage: %s <tsvc_func> <data_type>' % (sys.argv[0])
    sys.exit(1)

def construct_call(cname):
    assert(tests.has_key(cname))
    v = tests[cname]
    return '%s(%s)' % (cname, string.join(v, ', '))

def main():
    if len(sys.argv) != 3:
        print_usage('incorrect number of arguments')

    ## sanity check arguments
    outname = sys.argv[1]
    call = sys.argv[2]
    if not tests.has_key(call):
        skeys = tests.keys()
        skeys.sort()
        print_usage('allowed values for <tsvc_func>: %s' % string.join(skeys, ' '))

    callstr = construct_call(call)

    driverf = open('./driver.c.in', 'r')
    driver = driverf.readlines()
    driverf.close()

    drivero = open(outname, 'w')
    for line in driver:
        line = line.replace('___CALL_PLACEHOLDER___', callstr)
        drivero.write(line)
    drivero.close()


if __name__ == '__main__':
    main()
